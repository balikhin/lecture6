package sbp.io;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.io.IOException;

public class MyNIOExampleTest
{
    public final MyNIOExample myNIOExample = new MyNIOExample();


    /**
     * Тест если:
     * - отсутвует физически
     * - является директорией
     * - является файлом
     * @throws IOException
     */
    @Test
    public void workWithFileNIOTest() throws IOException
    {
        Assertions.assertFalse(myNIOExample.workWithFileNIO("C:\\NotExistDir"));
        Assertions.assertFalse(myNIOExample.workWithFileNIO("C:\\1"));
        Assertions.assertFalse(myNIOExample.workWithFileNIO("C:\\1\\Test2.txt"));
    }

    /**
     * Тест если:
     * - переданые данные не являются файлом (2 теста)
     * - копия файла успешно создана
     * @throws IOException
     */
    @Test
    public void copyFileNIOTest() throws IOException
    {
        Assertions.assertFalse(myNIOExample.copyFileNIO("C:\\1\\Test5.txt", "C:\\1\\TestCopy5.txt"));
        Assertions.assertFalse(myNIOExample.copyFileNIO("C:\\1", "C:\\1\\TestCopy5.txt"));
        Assertions.assertTrue(myNIOExample.copyFileNIO("C:\\1\\Test2.txt", "C:\\1\\TestCopy2.txt"));
    }

    /**
     * - переданые данные не являются файлом (2 теста)
     * - копия файла успешно создана
     * @throws IOException
     */
    @Test
    public void copyFileWitInputOutputStreamNIOTest() throws IOException
    {
        Assertions.assertFalse(myNIOExample.copyFileWithInputOutputStreamNIO("C:\\1\\Test5.txt", "C:\\1\\TestCopy5.txt"));
        Assertions.assertFalse(myNIOExample.copyFileWithInputOutputStreamNIO("C:\\1", "C:\\1\\TestCopy5.txt"));
        Assertions.assertTrue(myNIOExample.copyFileWithInputOutputStreamNIO("C:\\1\\Test2.txt", "C:\\1\\TestCopy2.txt"));
    }

    /**
     * - переданые данные не являются файлом (2 теста)
     * - копия файла успешно создана
     * @throws IOException
     */
    @Test
    public void copyBufferedFileTextNIO() throws IOException
    {
        Assertions.assertTrue(myNIOExample.copyFileBufferedNIO("C:\\1\\Test5.txt", "C:\\1\\TestCopy5.txt"));
        Assertions.assertFalse(myNIOExample.copyFileBufferedNIO("C:\\1", "C:\\1\\TestCopy5.txt"));
        Assertions.assertTrue(myNIOExample.copyFileBufferedNIO("C:\\1\\Test2.txt", "C:\\1\\TestCopy2.txt"));
    }
}