package sbp.jdbc;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import sbp.jdbc.dao.WorkWithTable;
import sbp.jdbc.model.Person;

import java.util.ArrayList;
import java.util.List;

public class JDBCTest
{
    final WorkWithTable personRepo = new WorkWithTable();
    final List<Person> list = new ArrayList<>();

    @Before
    public void start() {
        list.add(new Person("Andrew", "Moscow", 22));
        list.add(new Person("Masha", "Omsk", 19));
        list.add(new Person("Oleg", "Sochy", 27));
    }

    /**
     * Тест добавления записей в базу данных
     * и поиска всех и по id
     */
    @Test
    public void addAndFindByIdAndFindAllTest() throws Exception
    {
        int add = 1;
        if (!personRepo.createPerson(list.get(0))) add = 0;
        if (!personRepo.createPerson(list.get(1))) add = 0;
        if (!personRepo.createPerson(list.get(2))) add = 0;

        Assertions.assertEquals(1, add);
        Assertions.assertEquals(list, personRepo.findAllPerson());
        Assertions.assertEquals(list.get(1), personRepo.findPersonById(2));
    }

    /**
     * Тест изменения записи в базе данных
     */
    @Test
    public void updateTest() throws Exception
    {
        personRepo.createPerson(list.get(0));
        personRepo.createPerson(list.get(1));
        personRepo.createPerson(list.get(2));

        Person person = personRepo.findPersonById(2);
        person.setAge(40);
        list.set(1, person);
        personRepo.updatePerson(person);

        Assertions.assertEquals(list.get(1), personRepo.findPersonById(2));
    }

    /**
     * Тест удаления записи из базы данных
     */
    @Test
    public void deleteTest() throws Exception
    {
        personRepo.createPerson(list.get(0));
        personRepo.createPerson(list.get(1));
        personRepo.createPerson(list.get(2));

        personRepo.deletePerson(2);
        list.remove(1);

        Assertions.assertEquals(list, personRepo.findAllPerson());
    }

    @After
    public void end() {
        personRepo.deleteTable();
    }
}