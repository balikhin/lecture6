package sbp.jdbc.model;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PersonTest {

    /**
     * Тест метода compareTo класса Person
     */
    @Test
    public void personCompareToTest() {
        List<Person> list = new ArrayList<>();
        list.add(new Person("Oksana", "Moscow", 30));
        list.add(new Person("Andrey", "Omsk", 22));
        list.add(new Person("Oleg", "Sochy", 27));
        list.add(new Person("Anna", "Chita", 24));
        list.add(new Person("Maks", "Moscow", 33));

        Collections.sort(list);

        Assertions.assertEquals("Person{name='Anna', city='Chita', age=24}", list.get(0).toString());
        Assertions.assertEquals("Person{name='Maks', city='Moscow', age=33}", list.get(1).toString());
        Assertions.assertEquals("Person{name='Oksana', city='Moscow', age=30}", list.get(2).toString());
        Assertions.assertEquals("Person{name='Andrey', city='Omsk', age=22}", list.get(3).toString());
        Assertions.assertEquals("Person{name='Oleg', city='Sochy', age=27}", list.get(4).toString());
    }

    /**
     * Тест методов equals и hashCode класса Person
     */
    @Test
    public void equalsAndHashCodeTest() {
        Person person1 = new Person("Vasiliy", "Rostov", 40);
        Person person2 = person1;
        Person person3 = new Person("Vasiliy", "Rostov", 40);
        Person person4 = new Person("Vasiliy", "Rostov", 39);

        Assertions.assertTrue(person1.equals(person2));
        Assertions.assertTrue(person1.hashCode() == person2.hashCode());

        Assertions.assertTrue(person1.equals(person3));
        Assertions.assertTrue(person1.hashCode() == person3.hashCode());

        Assertions.assertTrue(person2.equals(person3));
        Assertions.assertTrue(person2.hashCode() == person3.hashCode());

        Assertions.assertFalse(person1.equals(person4));
        Assertions.assertFalse(person1.hashCode() == person4.hashCode());
    }
}
