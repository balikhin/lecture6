package sbp.jdbc.service;

public class CloseResources implements AutoCloseable
{
    @Override
    public void close() throws Exception
    {
        System.out.println("Resources Closed ");
    }
}