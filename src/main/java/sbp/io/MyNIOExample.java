package sbp.io;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class MyNIOExample
{
    /**
     * проверяется существование и чем является.
     * Если сущность существует, то выводит в консоль информацию:
     *      - абсолютный путь
     *      - родительский путь
     * Если сущность является файлом, то выводит в консоль:
     *      - размер
     *      - время последнего изменения
     * @param fileName - имя файла
     * @return - true, если файл успешно создан
     * @throws IOException
     */
    public boolean workWithFileNIO(String fileName) throws IOException
    {
        if (fileName.isEmpty()) return false;

        Path path = Paths.get(fileName);

        if (!Files.exists(path)) return false;

        System.out.println("Абсолютный путь: " + path.toAbsolutePath());
        System.out.println("Относительный путь: " + path.getRoot().relativize(path));

        if (Files.isDirectory(path)) return false;

        System.out.println("Размер файла: " + Files.size(path) + " байт.");
        System.out.println("Время последнего изменения: " + Files.getLastModifiedTime(path));

        return true;
    }



    /**
     * Метод создает копию файла
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     * @throws IOException
     */
    public boolean copyFileNIO(String sourceFileName, String destinationFileName) throws IOException
    {
        if (sourceFileName.isEmpty()) return false;

        if (!Files.exists(Paths.get(sourceFileName))) return false;
        if (Files.isDirectory(Paths.get(sourceFileName))) return false;
        if (Files.exists(Paths.get(destinationFileName))) throw new IOException("File " + destinationFileName + " already exist!");

        Files.copy(Paths.get(sourceFileName), Paths.get(destinationFileName));

        return true;
    }


    /**
     * Метод создает копию файда
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyFileWithInputOutputStreamNIO(String sourceFileName, String destinationFileName) throws IOException
    {
        if (sourceFileName.isEmpty()) return false;

        if (!Files.exists(Paths.get(sourceFileName))) return false;
        if (Files.isDirectory(Paths.get(sourceFileName))) return false;
        if (Files.exists(Paths.get(destinationFileName))) throw new IOException("File " + destinationFileName + " already exist!");

        try (InputStream in = Files.newInputStream(Paths.get(sourceFileName));
                OutputStream out = Files.newOutputStream(Paths.get(destinationFileName))) {

            int character;
            while ((character = in.read()) != -1)
            {
                out.write(character);
            }

            return true;
        }
    }

    /**
     * Метод создает копию файла
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyFileBufferedNIO(String sourceFileName, String destinationFileName) throws IOException
    {
        if (sourceFileName.isEmpty()) return false;

        if (!Files.exists(Paths.get(sourceFileName))) return false;
        if (Files.isDirectory(Paths.get(sourceFileName))) return false;
        if (Files.exists(Paths.get(destinationFileName))) throw new IOException("File " + destinationFileName + " already exist!");

        try (BufferedReader reader = new BufferedReader(Files.newBufferedReader(Paths.get(sourceFileName)));
             BufferedWriter writer = new BufferedWriter(Files.newBufferedWriter(Paths.get(destinationFileName))))
        {
            String line;
            while ((line = reader.readLine()) != null)
            {
                writer.write(line);
            }

            return true;
        }
    }
}
